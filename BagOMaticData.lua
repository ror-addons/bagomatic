-- BagOMatic Data

BagOMaticData = {}


-- Stolen from TokenMachine :D
BagOMaticData.MEDALLIONS = {
	[208400] = true, -- Recruit's Medallion
	[208401] = true, -- Scout's Medallion
	[208402] = true, -- Soldier's Medallion
	[208403] = true, -- Officer's Medallion
	[208404] = true, -- Conquerer's Crest
	[208405] = true, -- Invader's Crest
	[208406] = true, -- Warlord's Crest
	[208407] = true, -- Royal Crest
	[208419] = true, -- Entangled Officer's Medallion
	[208421] = true, -- Fused Conqueror's Crest
	[208429] = true, -- Invader's Crest [New]
	
	-- ADDED FOR RoR
	[1600] = true, -- Vanquisher's Medallion
	[1698] = true, -- Conquerer's Medallion
	[208404] = true, -- Invader Medallion
	[208431] = true, -- Recruit's Emblem
	[208433] = true, -- Soldier's Emblem
	[208434] = true, -- Officer's Emblem
	
	[80001] = true, -- Bestial Token
	[2166] = true, -- Seal of the Paragon
	[2165] = true, -- Seal of the Exemplar
	[1298378661] = true, -- Keeper's Karl
	[129838572] = true, -- Remnant of Ruin
}


BagOMaticData.LOOT_BAGS = {
	[9941] = true,	-- Lesser (green)
	[9942] = true,	-- Greater (blue)
	[9943] = true,	-- Major (purple)
	[9980] = true,	-- Massive (gold)
}


BagOMaticData.STANDARDS = {
	[187701] = true,
	[187702] = true,
	[187703] = true,
	[187704] = true,	-- Warped
	[187705] = true,	-- Bloodied
	[187706] = true,	-- Corruptor's
}


-- 11838 Light Oil Immunity Potion
BagOMaticData.SIEGE_WEAPONS = {
	-- T2
	[86215] = true,		-- Light Destruction Oil
	-- Chaos
	[8351] = true,		-- Light Reinforced Chaos Ram
	[8369] = true,		-- Light Chaos Bedlam Hellcannon
	[86213] = true,		-- Light Chaos Hellcannon
	[86214] = true,		-- Light Chaos Ram
	[86216] = true,		-- Light Chaos Hellblaster
	-- Greenskin
	[8353] = true,		-- Light Reinforced Greenskin Ram
	[8371] = true,		-- Light Greenskin Supa-Dupa Chucka
	[86237] = true,		-- Light Orcapult
	[86238] = true,		-- Light Greenskin Ram
	[86240] = true,		-- Light Greenskin Supa-Chucka

	-- T4
	[86223] = true,		-- Destruction Oil
	-- Chaos
	[8363] = true,		-- Heavy Reinforced Chaos Ram
	[8381] = true,		-- Heavy Chaos Bedlam Hellcannon
	[86221] = true,		-- Hellcannon
	[86222] = true,		-- Chaos Ram
	[86224] = true,		-- Chaos Hellblaster
	-- Greenskin
	[8365] = true,		-- Heavy Reinforced Greenskin Ram
	[8383] = true,		-- Heavy Greenskin Supa-Dupa Chucka
	[86245] = true,		-- Heavy Orcapult
	[86246] = true,		-- Heavy Greenskin Ram
	[86248] = true,		-- Heavy Greenskin Supa-Chucka
}


-- our custom ItemTypes
BagOMaticData.APOTHECARY = 1002
BagOMaticData.CULTIVATION = 1003
BagOMaticData.TALISMAN_MAKING = 1004
BagOMaticData.SALVAGING = 1005

BagOMaticData.WEAPON = 10000
BagOMaticData.ARMOR = 10001

BagOMaticData.STANDARD = 20004
BagOMaticData.LOOTBAG = 20005
BagOMaticData.MEDALLION = 20006
BagOMaticData.SCROLL = 20007
BagOMaticData.MOUNT = 20008

BagOMaticData.BOOKOFBINDING = 100000


BagOMaticData.CRAFTING_TYPE_MAP = {
	[GameData.TradeSkills.APOTHECARY] = BagOMaticData.APOTHECARY,
	[GameData.TradeSkills.SALVAGING] = BagOMaticData.SALVAGING,
	[GameData.TradeSkills.TALISMAN] = BagOMaticData.TALISMAN_MAKING,
}


function BagOMaticData.is_mount(item)
	return (item.uniqueID > 186800 and item.uniqueID < 186849) or (item.uniqueID > 208008 and item.uniqueID < 208050)
end

function BagOMaticData.is_scroll(item)
	return item.uniqueID > 65823 and item.uniqueID < 65830
end

function BagOMaticData.is_talisman(item)
	return item.uniqueID > 907000 and item.uniqueID < 907500
end

function BagOMaticData.is_bookofbinding(item)
	return item.uniqueID == 11919
end


function BagOMaticData.get_crafting_type(item)
	for _, bonus in ipairs(item.craftingBonus) do
		if (bonus.bonusReference == GameData.CraftingBonusRef.TYPE) then
			return bonus.bonusValue
		end
	end
	return nil	-- nothing found
end


function BagOMaticData.get_crafting_family(item)
	if (item.cultivationType > 0) then
		return BagOMaticData.CULTIVATION
	end
	for _, bonus in ipairs(item.craftingBonus) do
		if (bonus.bonusReference == GameData.CraftingBonusRef.CRAFTING_FAMILY) then
			return BagOMaticData.CRAFTING_TYPE_MAP[bonus.bonusValue] or d("New family found")
		end
	end
	-- nothing found, just return .type
	return item.type
end



function BagOMaticData.get_item_type(item)
	if (DataUtils.ItemIsWeapon(item)) then
		return BagOMaticData.WEAPON
	elseif (DataUtils.ItemIsArmor(item) and item.type ~= GameData.ItemTypes.ACCESSORY) then
		return BagOMaticData.ARMOR
	elseif (#item.craftingBonus > 0) then
		return BagOMaticData.get_crafting_family(item)
--[[
	elseif (DataUtils.IsTradeSkillItem(item, GameData.TradeSkills.CULTIVATION)) then
		return BagOMaticData.CULTIVATION
	elseif (DataUtils.IsTradeSkillItem(item, GameData.TradeSkills.APOTHECARY)) then
		return BagOMaticData.APOTHECARY

	elseif (DataUtils.IsTradeSkillItem(item, GameData.ItemTypes.EITEM_TYPE_SALVAGING)) then
		return BagOMaticData.SALVAGING
]]--
	elseif (BagOMaticData.is_talisman(item)) then
		return BagOMaticData.TALISMAN_MAKING
	-- TODO bigtable maybe
	elseif (BagOMaticData.MEDALLIONS[item.uniqueID]) then
		return BagOMaticData.MEDALLION
	elseif (BagOMaticData.is_mount(item)) then
		return BagOMaticData.MOUNT
	elseif (BagOMaticData.is_scroll(item)) then
		return BagOMaticData.SCROLL
	elseif (BagOMaticData.is_bookofbinding(item)) then
		return BagOMaticData.BOOKOFBINDING
	elseif (BagOMaticData.LOOT_BAGS[item]) then
		return BagOMaticData.LOOTBAG
	elseif (BagOMaticData.STANDARDS[item]) then
		return BagOMaticData.STANDARD
	else
		return item.type
	end
end

-- Special thanks to EA Mythic for NOT having such API...
function BagOMaticData.get_max_stackcount(item)
	if (BagOMaticData.MEDALLIONS[item.uniqueID]) then
		return 999				-- medallions 999
		
	elseif ((item.type == GameData.ItemTypes.DYE) or
		(item.type == GameData.ItemTypes.POTION) or 
		BagOMaticData.is_talisman(item)) then
		return 40		-- potions, dyes, talismans 40
		
	elseif (#item.craftingBonus > 0) then
		local crafttype = BagOMaticData.get_crafting_type(item)
		if 	(crafttype == GameData.CraftingItemType.STABILIZER) or
			(crafttype == GameData.CraftingItemType.CURIOS and item.craftingSkillRequirement <= 1) then
			return 50				-- stabilizers and level 1 Curios 50
		else
			return 40			-- everything else 40
		end
		
	elseif (BagOMaticData.is_scroll(item)) then
		return 5				-- recall scrolls 5
	else
		return 1
	end
end
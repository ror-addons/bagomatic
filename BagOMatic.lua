-- BagOMatic.lua
BagOMatic = {}
BagOMatic.VERSION = "v1.3.6"  -- fixed by Soulztorm (Hobbitschubsa), original addon credit: Daniel Tralamazza <tralamazza@gmail.com>
BagOMatic.POCKET_SIZE = 16
BagOMatic.STANDARD_DELAY = 0.15 --Default delay in seconds between item moves
								-- can be changed with /bom delay N   in game

-- our computation loop to emulate a sleep between item move requests
--function sleep()
--	local l = BagOMatic.delay * 1000000 for i=1,l do a = 1+1 end
--end

-- guess we don't need this stupid sh*t anymore :)

function BagOMatic.init()
	BagOMatic.debugon = false
	BagOMatic.fields_to_compare = { "bagomatic_type", "craftingSkillRequirement", "iLevel", "rarity", "equipSlot", "name", "stackCount" } -- BagOMatic.craft_compare, "typetext", "name"
	
	-- temp fix delay (blocking loop count = delay * 1 000 000)
	BagOMatic.delay_new = BagOMatic.STANDARD_DELAY

	-- for mass buy and sell function
	BagOMatic.HEARTBEAT = 0.001				-- period in seconds
	BagOMatic.elapsed = 0
	BagOMatic.cmd_queue = {}				-- global command queue
	BagOMatic.buysell_enabled = false
	BagOMatic.MAX_BUY_SELL = 10000
	BagOMatic.BUY = 1
	BagOMatic.SELL = 2
	
	BagOMatic.sorting = false
	BagOMatic.move_requests = {}
	BagOMatic.move_index = 1
	
	BagOMatic.buysalvage_enabled = false
	BagOMatic.salvage_hacked = false
	BagOMatic.SALVAGE_TIMEOUT = 4
	BagOMatic.DEFAULT_TIMEOUT = 1
	BagOMatic.MAX_BUY_SALVAGE = 100

	BagOMatic.cmd_current = nil			-- current queue command to be executed
	
--	BagOMatic.print("where am i?")
	-- salvaging hooks
--	if (SalvagingWindow) then
--		BagOMatic.print("I see you are a salvager")
--		BagOMatic.SALVAGE = 3
--		BagOMatic.original_Salvage = SalvagingWindow.Salvage
--		SalvagingWindow.Salvage = BagOMatic.SalvageHook
--	else
--		BagOMatic.print("I see you arent a salvager")
--	end
	BagOMatic.current_item_merchant_slot = -1

	-- backpack hooks
	BagOMatic.original_ToggleFilterChoice = EA_Window_Backpack.ToggleFilterChoice
	EA_Window_Backpack.ToggleFilterChoice = BagOMatic.ToggleFilterChoiceFunction

	-- persistent data
	BagOMatic.saved = BagOMatic.saved or {}
	if (BagOMatic.saved.current_filters == nil) then
		BagOMatic.reset_current_filters()
	end
	BagOMatic.saved.filters = BagOMatic.saved.filters or {}
	BagOMatic.saved.default_filters = BagOMatic.saved.default_filters or nil
	
	if (BagOMatic.saved.delay_new ~= nil) then
		BagOMatic.delay_new = BagOMatic.saved.delay_new
	else
		BagOMatic.delay_new = BagOMatic.STANDARD_DELAY
		BagOMatic.saved.delay_new = BagOMatic.STANDARD_DELAY
	end

	-- libslash commands
	BagOMatic.cmd = {}
	BagOMatic.cmd[""] = { ["f"] = BagOMatic.cmd_usage, ["help"] = "", ["print_id"] = 10 , ["print"] = false}
	BagOMatic.cmd["help"] = { ["f"] = BagOMatic.cmd_usage, ["help"] = "show command list", ["print_id"] = 11, ["print"] = true }
	BagOMatic.cmd["delay"] = { ["f"] = BagOMatic.cmd_delay, ["help"] = "set delay between item moves (default 0.15 secs)", ["print_id"] = 12, ["print"] = true }

	BagOMatic.cmd["sort"] = { ["f"] = BagOMatic.cmd_sort, ["help"] = "sort backpack items", ["print_id"] = 20, ["print"] = true }
	BagOMatic.cmd["sortbank"] = { ["f"] = BagOMatic.cmd_sortbank, ["help"] = "sort bank items", ["print_id"] = 21, ["print"] = true }
	BagOMatic.cmd["sortcraft"] = { ["f"] = BagOMatic.cmd_sortcraft, ["help"] = "sort crafting items", ["print_id"] = 22, ["print"] = true }
	BagOMatic.cmd["sortcurrency"] = { ["f"] = BagOMatic.cmd_sortcurrency, ["help"] = "sort currency items", ["print_id"] = 23, ["print"] = true }
	BagOMatic.cmd["sortall"] = { ["f"] = BagOMatic.cmd_sortall, ["help"] = "sort all backpacks", ["print_id"] = 24, ["print"] = true }

	BagOMatic.cmd["pack"] = { ["f"] = BagOMatic.cmd_pack, ["help"] = "remove empty spaces from your backpack", ["print_id"] = 30, ["print"] = true }
	BagOMatic.cmd["packbank"] = { ["f"] = BagOMatic.cmd_pack, ["help"] = "remove empty spaces from your bank", ["print_id"] = 31, ["print"] = true }
	BagOMatic.cmd["bank"] = { ["f"] = BagOMatic.cmd_showbank, ["help"] = "open (cached version) bank window", ["print_id"] = 40, ["print"] = true }

	BagOMatic.cmd["save"] = { ["f"] = BagOMatic.cmd_save_filters, ["help"] = "<name> save selected filter settings" , ["print_id"] = 50, ["print"] = false}
	BagOMatic.cmd["reset"] = { ["f"] = BagOMatic.cmd_reset_filters, ["help"] = "reset/clean selected filters" , ["print_id"] = 51, ["print"] = true} 
	BagOMatic.cmd["list"] = { ["f"] = BagOMatic.cmd_list_filters, ["help"] = "list saved filters" , ["print_id"] = 52, ["print"] = false}
	BagOMatic.cmd["load"] = { ["f"] = BagOMatic.cmd_load_filters, ["help"] = "<name> load saved filter settings" , ["print_id"] = 53, ["print"] = false}
	BagOMatic.cmd["clear"] = { ["f"] = BagOMatic.cmd_clear_filters, ["help"] = "clear all saved filters; <name> clears saved filter" , ["print_id"] = 54, ["print"] = false}

	BagOMatic.cmd["debug"] = { ["f"] = BagOMatic.cmd_debug, ["help"] = "debug flag", ["print_id"] = 80, ["print"] = false } -- toggle debug mode
	BagOMatic.cmd["id"] = { ["f"] = BagOMatic.cmd_item_id, ["help"] = "prints the item id", ["print_id"] = 81, ["print"] = false }

	-- for mass buy and sell function
	BagOMatic.cmd["bs"] = { ["f"] = BagOMatic.cmd_buysell, ["help"] = "mass buy and sell, <num> mass number, if num <0 or > 10000 then stops", ["print_id"] = 100, ["print"] = false } 
	BagOMatic.cmd["bsa"] = { ["f"] = BagOMatic.cmd_buysalvage, ["help"] = "mass buy and salvage, <num> mass number, if num <0 or > 10000 then stops", ["print_id"] = 100, ["print"] = false } 

	BagOMatic.cmd_ordered = {}
	for cmdkey, cmd in pairs(BagOMatic.cmd) do
		cmd.key = cmdkey
		table.insert(BagOMatic.cmd_ordered, cmd)
	end
	table.sort(BagOMatic.cmd_ordered, function(cm1, cm2) return cm1.print_id < cm2.print_id end)

	-- register libslash
	LibSlash.RegisterSlashCmd("bagomatic", function (msg) BagOMatic.parse_cmd(msg) end)
	BagOMatic.has_shortslash = false
	if (not LibSlash.IsSlashCmdRegistered("bom")) then
		LibSlash.RegisterSlashCmd("bom", function (msg) BagOMatic.parse_cmd(msg) end)
		BagOMatic.has_shortslash = true
	end

	-- load message
	local motd = "Version " .. BagOMatic.VERSION .. " loaded. Type /bagomatic"
	if (BagOMatic.has_shortslash) then
		motd = motd .. " or /bom"
	end
	motd = motd .. " for instructions"
	BagOMatic.print(motd)

	BagOMatic.original_Tooltips_CreateItemTooltip = Tooltips.CreateItemTooltip
	Tooltips.CreateItemTooltip = BagOMatic.Tooltips_CreateItemTooltip

	RegisterEventHandler(SystemData.Events.LOADING_END, "BagOMatic.restore_filters")
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "BagOMatic.restore_filters")

	if DoesWindowExist("EA_Window_Backpack") then
		CreateWindow("BagOMaticButton", true)
		WindowSetParent("BagOMaticButton", "EA_Window_Backpack")
		WindowAddAnchor("BagOMaticButton", "left", "EA_Window_BackpackTitleBarText", "right", 30, -2)
	end

	BagOMatic.debug("BagOMatic " .. BagOMatic.VERSION .. " loaded")
end


-- small hack to show uniqueID all items
function BagOMatic.Tooltips_CreateItemTooltip(itemData, mouseoverWindow, anchor, disableComparison, extraText, extraTextColor, ignoreBroken)
	local ext = L" (" .. itemData.uniqueID .. L")"
	if (extraText) then
		extraText = extraText .. ext
	else
		extraText = ext	
	end
	return BagOMatic.original_Tooltips_CreateItemTooltip(itemData, mouseoverWindow, anchor, disableComparison, extraText, extraTextColor, ignoreBroken)
end

function BagOMatic.cmd_delay(delay)
	if (delay == nil or #delay == 0) then
		BagOMatic.print("Current delay set to: " .. BagOMatic.delay_new)
	end
	local num = tonumber(delay[1])
	if (num ~= nil and type(num) == "number") then
		BagOMatic.delay_new = num
		BagOMatic.saved.delay_new = num
		BagOMatic.print("Delay between item moves set to: " .. num .. " seconds.")
	end
end


function BagOMatic.parse_cmd(msg)
	msg = msg:lower()
	local args = StringSplit(msg)
	cmd = table.remove(args, 1)
	if (BagOMatic.cmd[cmd] ~= nil) then
		BagOMatic.cmd[cmd].f(args)
	else
		BagOMatic.print("Unknown command: " .. msg)
	end
end


function BagOMatic.cmd_usage(args)
	local ab = "/bagomatic "
	if (BagOMatic.has_shortslash) then
		ab = "/bom "
	end
	BagOMatic.print("[Commands]")
	for _, cmd in ipairs(BagOMatic.cmd_ordered) do
		if ((cmd.help ~= "" and cmd.print) or BagOMatic.debugon) then
			BagOMatic.print(ab .. cmd.key .. " - " .. cmd.help)
		end
	end
end


function BagOMatic.cmd_sort(args)
	if (BagOMatic.sorting == false) then
		BagOMatic.print("Sorting inventory, please wait.")
		BagOMatic.sort_content(GetInventoryItemData(), Cursor.SOURCE_INVENTORY, Cursor.SOURCE_INVENTORY, true)
		BagOMatic.sorting = true -- set sorting flag for update method
	else
		BagOMatic.print("Already working, please wait.")
	end
end

function BagOMatic.cmd_sortall(args)
	if (BagOMatic.sorting == false) then
		BagOMatic.print("Sorting everything, please wait.")
		BagOMatic.sort_content(GetInventoryItemData(), Cursor.SOURCE_INVENTORY, Cursor.SOURCE_INVENTORY, true)
		BagOMatic.sort_content(GetCraftingItemData(), Cursor.SOURCE_CRAFTING_ITEM, Cursor.SOURCE_CRAFTING_ITEM, false)
		BagOMatic.sort_content(GetCurrencyItemData(), Cursor.SOURCE_CURRENCY_ITEM, Cursor.SOURCE_CURRENCY_ITEM, false)
		
		if (BankWindow.IsShowing()) then -- also sort bank if open
			BagOMatic.sort_content(GetBankData(), Cursor.SOURCE_BANK, Cursor.SOURCE_BANK, true)
			BagOMatic.print("Found open bank window, also sorting bank.")
		end
		BagOMatic.sorting = true -- set sorting flag for update method
	else
		BagOMatic.print("Already working, please wait.")
	end
end


function BagOMatic.cmd_sortbank(args)
	if (BagOMatic.sorting == false) then
		BagOMatic.print("Sorting bank, please wait.")
		BagOMatic.sort_content(GetBankData(), Cursor.SOURCE_BANK, Cursor.SOURCE_BANK, true)
		BagOMatic.sorting = true -- set sorting flag for update method
	else
		BagOMatic.print("Already working, please wait.")
	end
end


function BagOMatic.cmd_sortcraft(args)
	if (BagOMatic.sorting == false) then
		BagOMatic.print("Sorting crafting bags, please wait.")
		BagOMatic.sort_content(GetCraftingItemData(), Cursor.SOURCE_CRAFTING_ITEM, Cursor.SOURCE_CRAFTING_ITEM, false)
		BagOMatic.sorting = true -- set sorting flag for update method
	else
		BagOMatic.print("Already working, please wait.")
	end
end


function BagOMatic.cmd_sortcurrency(args)
	if (BagOMatic.sorting == false) then
		BagOMatic.print("Sorting currency, please wait.")
		BagOMatic.sort_content(GetCurrencyItemData(), Cursor.SOURCE_CURRENCY_ITEM, Cursor.SOURCE_CURRENCY_ITEM, false)
		BagOMatic.sorting = true -- set sorting flag for update method
	else
		BagOMatic.print("Already working, please wait.")
	end
end


function BagOMatic.cmd_sortguildvault(args)
	if (BagOMatic.sorting == false) then
		BagOMatic.print("Sorting guild vault, please wait.")
		BagOMatic.sort_content(GetGuildVault1ItemData(), Cursor.SOURCE_GUILD_VAULT1, Cursor.SOURCE_GUILD_VAULT1, true)
		BagOMatic.sort_content(nil, Cursor.SOURCE_GUILD_VAULT2, Cursor.SOURCE_GUILD_VAULT2, true)
		BagOMatic.sort_content(nil, Cursor.SOURCE_GUILD_VAULT3, Cursor.SOURCE_GUILD_VAULT3, true)
		BagOMatic.sort_content(nil, Cursor.SOURCE_GUILD_VAULT4, Cursor.SOURCE_GUILD_VAULT4, true)
		BagOMatic.sorting = true -- set sorting flag for update method
	else
		BagOMatic.print("Already working, please wait.")
	end
end

function BagOMatic.cmd_pack(args)
	if (BagOMatic.sorting == false) then
		BagOMatic.print("Packing inventory, please wait.")
		BagOMatic.pack_content(GetInventoryItemData(), Cursor.SOURCE_INVENTORY, Cursor.SOURCE_INVENTORY)
		BagOMatic.sorting = true -- set sorting flag for update method
	else
		BagOMatic.print("Already working, please wait.")
	end
end

function BagOMatic.cmd_packbank(args)
	if (BagOMatic.sorting == false) then
		BagOMatic.print("Packing bank, please wait.")
		BagOMatic.pack_content(GetBankData(), Cursor.SOURCE_BANK, Cursor.SOURCE_BANK)
		BagOMatic.sorting = true -- set sorting flag for update method
	else
		BagOMatic.print("Already working, please wait.")
	end
end


function BagOMatic.cmd_showbank(args)
	BankWindow.Show()
end


function BagOMatic.find_matching_pockets(item)
	local pockets = {}
	for _, pocket in ipairs(EA_Window_Backpack.pockets.filterOrder) do
		if (EA_Window_Backpack.DoesItemMatchPocketFilters(item, pocket)) then
			table.insert(pockets, pocket)
		end
	end
	return pockets
end


function BagOMatic.cmd_debug(args)
	BagOMatic.debugon = not BagOMatic.debugon
	BagOMatic.print("Debug mode: " .. tostring(BagOMatic.debugon))
end

function BagOMatic.cmd_item_id(args)
	if (BagOMatic.debugon) then
		local slot_id = 1
		if (args[1] ~= nil) then
			local n = tonumber(args[1])
			if (n < 0 or n >80) then
				BagOMatic.print("[error] Invalid slot number: " .. n)
				return
			end
			slot_id = math.floor(n)
		end
		local item = GetInventoryItemData(slot_id)
		BagOMatic.print("Item " .. tostring(item.name) .. " " .. item.uniqueID)
	end
end

function BagOMatic.pack_content(allitems, src_wnd, dst_wnd)
	local EMPTY_ITEM = { ["uniqueID"] = 0 }
	local unique_items = {}
	local unique_item = nil
	local empty_slots = {}
	
	--for slot, item in ipairs(allitems) do
	for slot=1,#allitems do
		local item = allitems[slot]
		
		item.src_slot = slot	-- put original slot number inside the item
		if (item.uniqueID <= 0) then	-- uniqueID == 0 equals empty
			table.insert(empty_slots, slot)
		else
			unique_item = unique_items[item.uniqueID]
			if (unique_item) then
				local stack_avail = BagOMaticData.get_max_stackcount(unique_item) - unique_item.stackCount
				if (stack_avail > 0) then
					BagOMatic.debug("consolidating " .. tostring(item.name) .. " from " .. slot .. " to " .. unique_item.src_slot .. " max stack " .. BagOMaticData.get_max_stackcount(unique_item) .. " dest stackcount " .. unique_item.stackCount .. " src stackCount " .. item.stackCount)
					
					local move_request = {}
					move_request.src_wnd = src_wnd
					move_request.src_slot = slot
					move_request.dst_wnd = dst_wnd
					move_request.dst_slot =  unique_item.src_slot
					move_request.stackCount = item.stackCount
					
					table.insert(BagOMatic.move_requests, move_request)
						
					--RequestMoveItem(src_wnd, slot, dst_wnd, unique_item.src_slot, item.stackCount)
					--sleep()
					
					unique_item.stackCount = unique_item.stackCount + math.min(stack_avail, item.stackCount)
					item.stackCount = item.stackCount - stack_avail		-- dec moved item
					if (item.stackCount > 0) then
						unique_items[unique_item.uniqueID] = item	-- reset unique
					else
						allitems[slot] = EMPTY_ITEM			-- empty
						table.insert(empty_slots, slot)
						item.stackCount = 0
					end
				else
					unique_items[unique_item.uniqueID] = item		-- reset unique
				end
			else
				unique_items[item.uniqueID] = item
			end
		end
	end
	
	
	local slot = #allitems
	local dst_slot = nil
	while (slot > 0 and #empty_slots > 0) do
		if (allitems[slot].uniqueID > 0) then
			if (slot > empty_slots[1]) then
				dst_slot = table.remove(empty_slots, 1)
				BagOMatic.debug(L"[pack] moving " .. allitems[slot].name .. L" from " .. slot .. L" to " .. dst_slot)
				--RequestMoveItem(src_wnd, slot, dst_wnd, dst_slot, allitems[slot].stackCount)
				--sleep()
				
				local move_request = {}
				move_request.src_wnd = src_wnd
				move_request.src_slot = slot
				move_request.dst_wnd = dst_wnd
				move_request.dst_slot =  dst_slot
				move_request.stackCount = allitems[slot].stackCount				
				table.insert(BagOMatic.move_requests, move_request)
				
					
				allitems[dst_slot], allitems[slot] = allitems[slot], allitems[dst_slot]		-- swap
			end
		end
		slot = slot - 1
	end
end


function BagOMatic.sort_content(allitems, src_wnd, dst_wnd, match_pockets)
	-- remove empty spaces
	BagOMatic.pack_content(allitems, src_wnd, dst_wnd)

	local sorted_items = {}
	for slot, item in pairs(allitems) do
		if (item.uniqueID > 0) then
			if (match_pockets) then
				item.matching_pockets = BagOMatic.find_matching_pockets(item)
			else
				item.matching_pockets = {}
			end
			item.src_slot = slot
			item.bagomatic_type = BagOMaticData.get_item_type(item)
			table.insert(sorted_items, item)
		end
	end

	-- sort ascending by matching pocket size
	table.sort(sorted_items, function(item1, item2) return #item1.matching_pockets < #item2.matching_pockets end)

	-- temporary pockets
	local pocket_candidates = {}
	for _, p in ipairs(EA_Window_Backpack.pockets.filterOrder) do
		pocket_candidates[p] = {}
	end

	local remainder = {}
	for slot, item in ipairs(sorted_items) do
		if (#item.matching_pockets > 0) then
			for i, p in ipairs(item.matching_pockets) do
				table.insert(pocket_candidates[p], item)	-- this can overflow, not a problem now (we need global sorting)
				break	-- found a place
			end
		else
			table.insert(remainder, item)	-- contain all non-filtered items
		end
	end

	sorted_items = {}	-- clear table, we will repopulate

	for p, pocket in pairs(pocket_candidates) do
		table.sort(pocket, BagOMatic.compare_items)	-- sort each pocket
		while (#pocket > BagOMatic.POCKET_SIZE) do
			local item = table.remove(pocket) -- remove excess
			table.remove(item.matching_pockets, 1) -- remove 1st pocket ("p")
			if (#item.matching_pockets > 0) then -- if we have other matching pockets to try
				table.insert(pocket_candidates[item.matching_pockets[1]], item) -- get the first one
			else
				table.insert(remainder, item) -- no matching pocket left
			end
		end
		-- process max BagOMatic.POCKET_SIZE items
		for i, item in ipairs(pocket) do
			local slot = ((p - 2) * BagOMatic.POCKET_SIZE) + i		-- "- 2" because pouches start at index 2 (quest items are 1)
			item.dst_slot = slot
			sorted_items[slot] = item
		end
	end

	table.sort(remainder, BagOMatic.compare_items)		-- sort remainders
	for slot, item in ipairs(allitems) do
		if (not sorted_items[slot]) then
			local r_item = table.remove(remainder, 1)
			if (not r_item) then
				break				-- no more remainders
			end
			sorted_items[slot] = r_item
			sorted_items[slot].dst_slot = slot
		end
	end
	
	local subst_item = nil
	for slot, item in pairs(sorted_items) do
		subst_item = allitems[slot]
		if (item ~= subst_item) then
			BagOMatic.debug(L"moving " .. item.name .. L" from " .. item.src_slot .. L" to " .. slot)
			
			local move_request = {}
			move_request.src_wnd = src_wnd
			move_request.src_slot =  item.src_slot
			move_request.dst_wnd = dst_wnd
			move_request.dst_slot =  item.dst_slot
			move_request.stackCount = item.stackCount				
			table.insert(BagOMatic.move_requests, move_request)

			--RequestMoveItem(src_wnd, item.src_slot, dst_wnd, item.dst_slot, item.stackCount)
			--sleep()
					
			allitems[item.dst_slot], allitems[item.src_slot] = allitems[item.src_slot], allitems[item.dst_slot]	-- swap
			if (subst_item.uniqueID ~= 0) then
				subst_item.src_slot = item.src_slot
			end
		end
	end
end


function BagOMatic.craft_compare(item1, item2)
	if (item1.craftingSkillRequirement > 0 and item2.craftingSkillRequirement > 0) then
		local cmp = 0
		for i, craftbonus1 in ipairs(item1) do
			if (craftingBonus.bonusReference ~= item2[i].bonusReference) then
				break
			end
			cmp = craftbonus1.bonusValue - item2[i].bonusValue
			if (cmp ~= 0) then
				return cmp > 0
			end
		end
	end
end


function BagOMatic.compare_items(item1, item2)
	local f1 = nil
	local f2 = nil
		
	for _, compareto in ipairs(BagOMatic.fields_to_compare) do
		if (type(compareto) == "string") then	
			f1 = item1[compareto]
			f2 = item2[compareto]
			if (f1 ~= f2) then
				return f1 > f2
			end
		else
			local ret = compareto(item1, item2)
			if (ret) then
				return ret
			end
		end
	end
end


function BagOMatic.update(elapsed)
	
	if (BagOMatic.sorting) then 
		BagOMatic.elapsed = BagOMatic.elapsed + elapsed
		if (BagOMatic.elapsed > BagOMatic.delay_new) then
			BagOMatic.elapsed = 0
			if (BagOMatic.move_index <= #BagOMatic.move_requests) then
				local mr = BagOMatic.move_requests[BagOMatic.move_index]
				RequestMoveItem(mr.src_wnd, mr.src_slot, mr.dst_wnd, mr.dst_slot, mr.stackCount)
				BagOMatic.move_index = BagOMatic.move_index + 1
				return
			else
				BagOMatic.print("Sorting completed.")
				BagOMatic.sorting = false
				BagOMatic.move_requests = {} --clear move list
				BagOMatic.move_index = 1
			end
		end
	else
		--BagOMatic.buysell_enabled = true
		if (not BagOMatic.buysell_enabled and not BagOMatic.buysalvage_enabled) then 
			return
		end
		if (BagOMatic.buysalvage_enabled and not BagOMatic.salvage_hacked) then
			return
		end

		BagOMatic.elapsed = BagOMatic.elapsed + elapsed
		if (BagOMatic.elapsed > BagOMatic.HEARTBEAT) then
			BagOMatic.elapsed = 0		-- just in case something goes wrong inside
			
		end

		if (BagOMatic.cmd_current == nil) then
			if (#BagOMatic.cmd_queue > 0) then
				BagOMatic.cmd_current = table.remove(BagOMatic.cmd_queue, 1)		-- remove first cmd
			else
				BagOMatic.buysell_enabled = false	-- no current and nothing to dequeue, just return
				BagOMatic.buysalvage_enabled = false	-- no current and nothing to dequeue, just return
				return				-- no current and nothing to dequeue, just return
			end
		end
		BagOMatic.cmd_current.ticks = BagOMatic.cmd_current.ticks - elapsed
		if (BagOMatic.cmd_current.ticks <= 0) then
		--	if (AutoBand.debugon or AutoBand.gizilon) then
		--		AB_util.debug(tostring(AutoBand.cmd_current.cmd))
		--	end
			local current = BagOMatic.cmd_current
			if (current.cmd == BagOMatic.BUY) then
				--BagOMatic.buyItem(current.item_id)
				BagOMatic.debug("executing BUY")
				BagOMatic.buyItem(BagOMatic.current_item_merchant_slot)
			elseif (current.cmd == BagOMatic.SELL) then
				BagOMatic.sellItemAt(current.slot_id)
			elseif (current.cmd == BagOMatic.SALVAGE) then
				--current.time
				BagOMatic.debug("executing SALVAGE")
				BagOMatic.salvageLastItem()
			else
				BagOMatic.print("so what?")	-- what?
			end
			
			local cmd_tmp = BagOMatic.cmd_current
			BagOMatic.cmd_current = nil	-- for security clear cmd_current before callback
		end --]]
	end
end


-- ####### [start] / Filter Functions #######
function BagOMatic.cmd_reset_filters()
	BagOMatic.reset_current_filters()
	BagOMatic.restore_filters()
end	


function BagOMatic.cmd_save_filters(args)	-- give a name to your filter
	if (args[1] == nil) then
		BagOMatic.print("[error] Missing argument")
		return
	end
	local filter = BagOMatic.getFilter()
	BagOMatic.saved.current_filters = filter
	BagOMatic.saved.default_filters = args[1]
	BagOMatic.saved.filters[args[1]] = filter
	BagOMatic.print("Save complete")
end


function BagOMatic.cmd_list_filters(args)
	if (BagOMatic.saved.default_filters ~= nil) then
		BagOMatic.print("default filter " .. BagOMatic.saved.default_filters)
	else
		BagOMatic.print("No default filter set")
	end
	for tname in pairs(BagOMatic.saved.filters) do
		BagOMatic.print(tname)
	end
end


function BagOMatic.cmd_load_filters(args)
	if (#args > 0) then
		if (BagOMatic.saved.filters[args[1]] == nil) then
			BagOMatic.print("[error] Filter " .. args[1] .. " does not exist")
			return
		end
		BagOMatic.saved.default_filters = args[1]
		BagOMatic.saved.current_filters = BagOMatic.saved.filters[BagOMatic.saved.default_filters]
		BagOMatic.restore_filters()
		BagOMatic.print("Default filter load: " .. BagOMatic.saved.default_filters)
	end
	
end


function BagOMatic.cmd_clear_filters(args)
	if (#args > 0) then
		if (BagOMatic.saved.filters[args[1]] == nil) then
			BagOMatic.print("[error] Filter " .. args[1] .. " does not exist")
			return
		end
		if (BagOMatic.saved.default_filters == args[1]) then
			BagOMatic.saved.default_filters = nil
		end
		BagOMatic.saved.filters[args[1]] = nil
		BagOMatic.print("Saved filter " .. args[1] .." cleared")
	else
		BagOMatic.saved.default_filters = nil
		BagOMatic.saved.filters = {}
		BagOMatic.print("All saved filters cleared")
	end
end


function BagOMatic.ToggleFilterChoiceFunction()
	BagOMatic.original_ToggleFilterChoice()

	local checkBoxName = SystemData.MouseOverWindow.name
	local filterChoice = WindowGetId(checkBoxName)
	local pocketNumber = EA_Window_Backpack.pocketForFiltersMenuOpened 

	local pocketData = EA_Window_Backpack.pockets[pocketNumber]
	BagOMatic.saved.current_filters[pocketNumber][filterChoice] = pocketData.filters[filterChoice]
end
-- ####### [end] / Filter Functions #######


-- ####### [start] / Utility Functions #######
function BagOMatic.debug(stuff)
	if (BagOMatic.debugon) then
		d(stuff)
	end
end


function BagOMatic.print(text)
	if text == nil then
		text = ""
	end
	EA_ChatWindow.Print(L"[BagOMatic] " .. towstring(text))
end


function BagOMatic.reset_current_filters() 
	BagOMatic.saved.current_filters = {[2] ={}, [3] ={}, [4] ={}, [5] ={}, [6] ={}}
end


function BagOMatic.restore_filters()
	BagOMatic.debug(BagOMatic.saved.current_filters)
	for pocketNumber, pocket in pairs(BagOMatic.saved.current_filters) do
		local pocketData = EA_Window_Backpack.pockets[pocketNumber]
		if (pocketData) then
			pocketData.filters = pocketData.filters or {}
			for filterID in pairs(pocket) do
				pocketData.filters[filterID] = true
			end
		end
	end
end


function BagOMatic.getFilter()
	local MainMenuIndex, SubMenuIndex
	local anyFilters = false
	local filterFunction, filterData
	local filters = {[2] ={}, [3] ={}, [4] ={}, [5] ={}, [6] ={}}
	local pockets = {}
	for _, pocketNumber in ipairs(EA_Window_Backpack.pockets.filterOrder) do
		local pocketData = EA_Window_Backpack.pockets[pocketNumber]
		for filterID in pairs(  pocketData.filters ) do
			filterName = EA_Window_Backpack.GetFilterNameFromID( filterID )
			MainMenuIndex, SubMenuIndex = EA_Window_Backpack.GetFilterIndicesFromID( filterID )
			filterData = EA_Window_Backpack_Filters.menus[MainMenuIndex][SubMenuIndex]
			BagOMatic.debug(filterID .. " " .. tostring(filterName) .. " " .. filterData.filterGroup)
			filters[pocketNumber][filterID] = true
		end
	end
	return filters
end
-- ####### [end] / Utility Functions #######


-- ####### [start] / Window Functions #######
function BagOMatic.wnd_on_lbutton_up()
	BagOMatic.cmd_sortall(nil)
end


function BagOMatic.wnd_on_mouse_over()
	Tooltips.CreateTextOnlyTooltip("BagOMaticButton", L"BagOMatic: Sort all")
	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_TOP)
end
-- ####### [end] / Window Functions #######


function BagOMatic.massBuySell(num)
	local slot_id = BagOMatic.find1stemptyslot()
	if (slot_id < 0) then
		BagOMatic.print("No empty space to buy data")
	end
	BagOMatic.print("Empty slot " .. slot_id)
	
	for i=1,num do
		--BagOMatic.buyLastItem()
		table.insert(BagOMatic.cmd_queue, {["cmd"] = BagOMatic.BUY})
		--BagOMatic.sellItemAt(slot_id)
		table.insert(BagOMatic.cmd_queue, {["cmd"] = BagOMatic.SELL, ["slot_id"]= slot_id})
		BagOMatic.buysell_enabled = true
	end

end

function BagOMatic.sellItemAt(slot_id)
	--DestroyItem(Cursor.SOURCE_INVENTORY, slot_id)
	EA_Window_InteractionStore.SellItem(slot_id, 1)
end

function BagOMatic.buyLastItem()
	--EA_Window_InteractionStore.BuyItem()
	-- EA_Window_InteractionStore.displayData 
	EA_Window_InteractionStore.ConfirmThenBuyItem(13, 1)
end

function BagOMatic.buyItem(item_slot_num)
	--EA_Window_InteractionStore.BuyItem()
	-- EA_Window_InteractionStore.displayData 
	EA_Window_InteractionStore.ConfirmThenBuyItem(item_slot_num, 1)
end



function BagOMatic.massBuySalvage(num)
--	if (not BagOMatic.SALVAGE) then
--		return
--	end
--	local slot_id = BagOMatic.find1stemptyslot()
--	if (slot_id < 0) then
--		BagOMatic.print("No empty space to buy data")
--	end
--	BagOMatic.print("Empty slot " .. slot_id)

--	local item_slot_id = BagOMatic.current_item_slot
--	if (BagOMatic.current_item_slot <0) then
--		BagOMatic.print("Current item cant be found ")
--		item_slot_id = 13
--	end
	
	for i=1,num do
		--BagOMatic.buyLastItem()
		table.insert(BagOMatic.cmd_queue, {["cmd"] = BagOMatic.BUY, ["ticks"]= BagOMatic.SALVAGE_TIMEOUT})
		--["item_id"]= item_slot_id, 
		--BagOMatic.sellItemAt(slot_id)
		table.insert(BagOMatic.cmd_queue, {["cmd"] = BagOMatic.SALVAGE, ["ticks"]=BagOMatic.DEFAULT_TIMEOUT })
		BagOMatic.buysalvage_enabled = true
	end

end
--SalvagingWindow.Salvage( slotNum )
-- small hack to get the data of the salvaged item
function BagOMatic.SalvageHook(slotNum)
	BagOMatic.debug(slotNum)
	--BagOMatic.print("SalvageHook slotNum: "..slotNum)
	local backpackType = EA_BackpackUtilsMediator.GetCurrentBackpackType()
	local inventory = EA_BackpackUtilsMediator.GetItemsFromBackpack( backpackType )
	local myItemData = inventory[slotNum]
	BagOMatic.current_item_name = string.sub(tostring(myItemData.name), 1, -1)
	--BagOMatic.print(BagOMatic.current_item_name)
	-- need some processing 
	--if (not BagOMatic.buysalvage_enabled) then

	BagOMatic.current_item_merchant_slot = BagOMatic.findItemInMerchant(myItemData.name)

	BagOMatic.original_Salvage(slotNum)
	BagOMatic.salvage_hacked = true
end


function BagOMatic.salvageLastItem(item_slot)
	-- find the first item in the bag pack
	-- or actually if SalvagingWindow.selectedSlot isnt the current item
	if (not BagOMatic.current_item_name) then
		BagOMatic.debug("ERROR 1")
	end
	SalvagingWindow.selectedSlot = BagOMatic.findItemInBagPack(BagOMatic.current_item_name)
	if (not SalvagingWindow.selectedSlot) then
		BagOMatic.debug("ERROR 2 was here")
	end
	SalvagingWindow.OnAcceptLButtonUp()
end

function BagOMatic.find1stemptyslot()
	local EMPTY_ITEM = { ["uniqueID"] = 0 }
	local allitems = GetInventoryItemData()
	for slot, item in ipairs(allitems) do
		if (item.uniqueID <= 0) then	-- uniqueID == 0 equals empty
			return slot
		end
	end
	return -1
end

function BagOMatic.salvage_hook()
	if (SalvagingWindow) then
		BagOMatic.SALVAGE = 3
		BagOMatic.original_Salvage = SalvagingWindow.Salvage
		SalvagingWindow.Salvage = BagOMatic.SalvageHook
		BagOMatic.print("SALVAGER FOUND")
	else
		BagOMatic.print("NO SALVAGER HOOK")
	end
end

function BagOMatic.isSalvager()
	-- GameData.TradeSkillLevels[GameData.TradeSkills.SALVAGING] and >0
	return (GameData.TradeSkillLevels[GameData.TradeSkills.SALVAGING] > 0)
end

function BagOMatic.findItemInMerchant(myItemData_Wname)
	for dataIdx, item in ipairs(EA_Window_InteractionStore.displayData) do
	--	BagOMatic.print(item.name .. L" == " ..myItemData_name)
	--	if (string.sub(tostring(item.name), 1, -1) == myItemData_name) then
	--		return item.slotNum
	--	end
		if (item.name == myItemData_Wname) then
			BagOMatic.debug("findItemInMerchant" .. item.slotNum)
			return item.slotNum
		end
	end
end
function BagOMatic.findItemInBagPack(myItemData_Sname)
	local backpackType = EA_BackpackUtilsMediator.GetCurrentBackpackType()
	local inventory = EA_BackpackUtilsMediator.GetItemsFromBackpack( backpackType )

	for dataIdx, item in ipairs(inventory) do
		--BagOMatic.print(tostring(item.name) .. " == ?" .. myItemData_Sname)
		if (string.sub(tostring(item.name), 1, -1) == myItemData_Sname) then
			BagOMatic.debug("findItemInBagPack" .. dataIdx)
			return dataIdx
		end
	end
end


function BagOMatic.cmd_stop_bs(args)
	BagOMatic.cmd_queue = {}
end


function BagOMatic.cmd_buysell(args)
	if (args[1] == nil) then
		BagOMatic.print("[error] Missing argument")
		return
	end
	local n = tonumber(args[1])
	if (n <= 0 or n > BagOMatic.MAX_BUY_SELL) then
		BagOMatic.print("[error] Invalid number: Stopping" .. n)
		BagOMatic.cmd_queue = {}
		return
	end

	BagOMatic.massBuySell(math.floor(n))
	BagOMatic.print("DONE ")
end


function BagOMatic.cmd_buysalvage(args)
	if (not BagOMatic.isSalvager()) then 
		BagOMatic.debug("NOT A SALVAGER ")
		return
	elseif (not BagOMatic.salvage_hacked) then
		-- hack the salvage function
		BagOMatic.salvage_hook()
	end

	if (args[1] == nil) then
		BagOMatic.print("[error] Missing argument")
		return
	end
	local n = tonumber(args[1])
	if (n <= 0 or n > BagOMatic.MAX_BUY_SALVAGE) then
		BagOMatic.print("[error] Invalid number: Stopping" .. n)
		BagOMatic.cmd_queue = {}
		return
	end
	--BagOMatic.current_item_slot = BagOMatic.findItemInMerchant("Decimator Feetsplate")
	--BagOMatic.print(BagOMatic.current_item_slot)
	BagOMatic.massBuySalvage(math.floor(n))
	--BagOMatic.print("DONE ")
end

